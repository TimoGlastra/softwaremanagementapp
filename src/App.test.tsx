import React from "react";
import { Text } from "react-native";
import App from "./App";

import renderer from "react-test-renderer";

describe("<App />", () => {
  it("renders correctly", () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders the correct amount of children", () => {
    const component = renderer.create(<App />);

    const textInstances = component.root.findAllByType(Text);
    expect(textInstances).toHaveLength(3);
  });
});
