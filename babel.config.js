// Because of a bug in react-native / babel / jest the presets can't be read
// from the .babelrc file. So the .babelrc extends this file, cause react-native needs .babelrc
module.exports = function(api) {
  api.cache(true);
  return {
    presets: ["module:metro-react-native-babel-preset"],
    plugins: ["@babel/plugin-transform-runtime"]
  };
};
