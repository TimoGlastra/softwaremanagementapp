describe("App", () => {
  it("should be visible", async () => {
    await device.reloadReactNative();
    await expect(element(by.id("app"))).toBeVisible();
  });
});
